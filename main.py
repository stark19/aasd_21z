import io
import time, keyboard, datetime
from queue import Queue

from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QThread
from spade import quit_spade
from agents.DriverAgent import DriverAgent
from agents.CustomerHandlerAgent import CustomerHandlerAgent
from agents.ParkingAgent import ParkingAgent
from agents.ParkingSpotAgent import ParkingSpotAgent
from agents.SensorAgent import SensorAgent
from agents.CameraAgent import CameraAgent
from test_cases import run_tests
from tools import get_my_ip
import sys
from PyQt5.QtWidgets import QApplication
from gui import MainWindow, OutLog

##### Obecnie używani klienci ####
kierowca1='kierowca1'
obsluga='obsluga'
parking_name=["parkingA","parkingB"]
spotA_name=["spotA1","spotA2"]
spotB_name=["spotB1","spotB2","spotB3"]
sensorA_name=["sensorA1","sensorA2"]
sensorB_name=["sensorB1"]
cameraA_name="cameraA"
cameraB_name="cameraB"
cameraB_spots_name=["spotB2","spotB3"]

##################################

global_password = '1'
cameraB_spots_state = ["F","F"]
parkingA_spots_state = [0,0]
parkingB_spots_state = [0,0,0]
parkingAB_state = [parkingA_spots_state, parkingB_spots_state]


mode = 1
# GUI mode 1
# Console test mode 0


class SystemAgents:
    driver = None
    customer_handler = None
    parking_list = []
    spotA_list = []
    spotB_list = []
    sensorA_list = []
    sensorB_list = []
    def __init__(self):
        self.prepare_agents()

    def prepare_agents(self):
        # sensors parking A
        for i in range(len(sensorA_name)):
            sensorA = SensorAgent(sensorA_name[i] + "@" + get_my_ip(), global_password, sensor_state="F",
                                  spot=spotA_name[i])
            self.sensorA_list.append(sensorA)
            future = sensorA.start()
            future.result()
        #sensors parking B
        for i in range(len(sensorB_name)):
            sensorB=SensorAgent(sensorB_name[i]+"@"+get_my_ip(), global_password, sensor_state="T", spot=spotB_name[i])
            self.sensorB_list.append(sensorB)
            future = sensorB.start()
            future.result()
        #camera parking B
        cameraB = CameraAgent(cameraB_name+"@"+get_my_ip(), global_password, parking_letter="B",
                              spots=cameraB_spots_name, spots_state=cameraB_spots_state)
        future = cameraB.start()
        future.result()

        # spots parking A
        for i in range(2):
            spotA = ParkingSpotAgent(spotA_name[i] + "@" + get_my_ip(), global_password, parking_letter="A",
                                     spot=spotA_name[i], sensor_or_camera=sensorA_name[i], spot_state="F")
            self.spotA_list.append(spotA)
            future = spotA.start()
            future.result()

        #spots parking B sensors
        for i in range(1):
            spotB=ParkingSpotAgent(spotB_name[i]+"@"+get_my_ip(),global_password,parking_letter="B",spot=spotB_name[i],sensor_or_camera=sensorB_name[i],spot_state="F")
            self.spotB_list.append(spotB)
            future = spotB.start()
            future.result()
        #spots parking B camera
        for i in range(1,3):
            spotB=ParkingSpotAgent(spotB_name[i]+"@"+get_my_ip(),global_password,parking_letter="B",spot=spotB_name[i],sensor_or_camera=cameraB_name,spot_state="F")
            self.spotB_list.append(spotB)
            future = spotB.start()
            future.result()

        # parking A
        parking = ParkingAgent(parking_name[0] + "@" + get_my_ip(), global_password, parking_spots=[*spotA_name,*spotB_name],
                               parking_letter="A", parking_spots_state=[*parkingA_spots_state,*parkingB_spots_state])
        self.parking_list.append(parking)
        future = parking.start()
        future.result()

        self.customer_handler = CustomerHandlerAgent(obsluga + "@" + get_my_ip(), global_password, parkings=["parkingA"],
                                               parkings_state=[["F", "F", "F", "F", "F"]],
                                               parkings_spots_delivery=[["N", "N","D", "N", "N"]],
                                               reservation_list=[[0, 0, 0, 1, 1, 0, 1, 2]])
        future = self.customer_handler.start()
        future.result()  # wait for receiver agent to be prepared.

        # driver
        self.driver = DriverAgent(kierowca1 + "@" + get_my_ip(), global_password, customer_handler_name=obsluga, driver_id="0",
                             is_delivery_man="N")
        future = self.driver.start()
        future.result()

# The new Stream Object which replaces the default stream associated with sys.stdout
# This object just puts data in a queue!
class WriteStream(object):
    def __init__(self, queue):
        self.queue = queue

    def write(self, text):
        self.queue.put(text)

# A QObject (to be run in a QThread) which sits waiting for data to come through a Queue.Queue().
# It blocks until data is available, and one it has got something from the queue, it sends
# it to the "MainThread" by emitting a Qt Signal
class MyReceiver(QObject):
    mysignal = pyqtSignal(str)

    def __init__(self, queue, *args, **kwargs):
        QObject.__init__(self, *args, **kwargs)
        self.queue = queue

    @pyqtSlot()
    def run(self):
        while True:
            text = self.queue.get()
            self.mysignal.emit(text)

# An example QObject (to be run in a QThread) which outputs information with print
class LongRunningThing(QObject):
    @pyqtSlot()
    def run(self):
        while True:
            pass

## GUI MODE(1) CAN ONLY WORK WITH ONE PARKING FOR PRESENTATION PURPOUSES ONLY

## CONSOLE MODE(0) CAN WORK WITH FULL SYSTEM AND IS NOW CONFIGURED TO RUN TESTS AND OUTPUT RESULTS IN CONSOLE

if __name__ == "__main__":
    if mode == 1:
        queue = Queue()
        sys.stderr = None # TO SUPRESS WEIRD DB COMMUNICATES
        sys.stdout = WriteStream(queue)
        app = QApplication(sys.argv)
        my_agents = SystemAgents()
        window = MainWindow(parking_spots=[[*spotA_name, *spotB_name]], driver=my_agents.driver, LongRunningThing=LongRunningThing)
        window.show()
        thread = QThread()
        my_receiver = MyReceiver(queue)
        my_receiver.mysignal.connect(window.console.append_text)
        my_receiver.moveToThread(thread)
        thread.started.connect(my_receiver.run)
        thread.start()
        app.exec_()
        print("Agents finished")
        quit_spade()

    elif mode == 0:
        sys.stderr = None # TO SUPRESS WEIRD DB COMMUNICATES
        run_tests()
        while True:
            if keyboard.is_pressed("q"):
                break
        print("Agents finished")
        quit_spade()