import time, keyboard, datetime
from spade import quit_spade
from agents.DriverAgent import DriverAgent
from agents.CustomerHandlerAgent import CustomerHandlerAgent
from agents.ParkingAgent import ParkingAgent
from agents.ParkingSpotAgent import ParkingSpotAgent
from agents.SensorAgent import SensorAgent
from agents.CameraAgent import CameraAgent
from tools import get_my_ip
import sys
from PyQt5.QtWidgets import QApplication
from gui import MainWindow

##### Obecnie używani klienci ####
kierowca1='kierowca1'
obsluga='obsluga'
parking_name=["parkingA","parkingB"]
spotA_name=["spotA1","spotA2"]
spotB_name=["spotB1","spotB2","spotB3"]
sensorA_name=["sensorA1","sensorA2"]
sensorB_name=["sensorB1"]
cameraA_name="cameraA"
cameraB_name="cameraB"
cameraB_spots_name=["spotB2","spotB3"]

##################################

global_password = '1'
cameraB_spots_state = ["F","F"]
parkingA_spots_state = [0,0]
parkingB_spots_state = [0,0,0]
parkingAB_state = [parkingA_spots_state, parkingB_spots_state]


class SystemAgents:
    driver = None
    customer_handler = None
    parking_list = []
    spotA_list = []
    spotB_list = []
    sensorA_list = []
    sensorB_list = []
    def __init__(self):
        self.prepare_agents()

    def prepare_agents(self):
        # sensors parking A
        for i in range(len(sensorA_name)):
            sensorA = SensorAgent(sensorA_name[i] + "@" + get_my_ip(), global_password, sensor_state="F",
                                  spot=spotA_name[i])
            self.sensorA_list.append(sensorA)
            future = sensorA.start()
            future.result()
        # sensors parking B
        for i in range(len(sensorB_name)):
            sensorB = SensorAgent(sensorB_name[i] + "@" + get_my_ip(), global_password, sensor_state="T",
                                  spot=spotB_name[i])
            self.sensorB_list.append(sensorB)
            future = sensorB.start()
            future.result()
        #camera parking B
        cameraB = CameraAgent(cameraB_name+"@"+get_my_ip(), global_password, parking_letter="B",
                              spots=cameraB_spots_name, spots_state=cameraB_spots_state)
        future = cameraB.start()
        future.result()

        # spots parking A
        for i in range(2):
            spotA = ParkingSpotAgent(spotA_name[i] + "@" + get_my_ip(), global_password, parking_letter="A",
                                     spot=spotA_name[i], sensor_or_camera=sensorA_name[i], spot_state="F")
            self.spotA_list.append(spotA)
            future = spotA.start()
            future.result()

        #spots parking B sensors
        spotB=ParkingSpotAgent(spotB_name[0]+"@"+get_my_ip(),global_password,parking_letter="B",spot=spotB_name[0],sensor_or_camera=sensorB_name[0],spot_state="F")
        self.spotB_list.append(spotB)
        future = spotB.start()
        future.result()
        #spots parking B camera
        for i in range(1,3):
            spotB=ParkingSpotAgent(spotB_name[i]+"@"+get_my_ip(),global_password,parking_letter="B",spot=spotB_name[i],sensor_or_camera=cameraB_name,spot_state="F")
            self.spotB_list.append(spotB)
            future = spotB.start()
            future.result()

        # parking A
        parking = ParkingAgent(parking_name[0] + "@" + get_my_ip(), global_password, parking_spots=spotA_name,
                               parking_letter="A", parking_spots_state=[*parkingA_spots_state])
        self.parking_list.append(parking)
        future = parking.start()
        future.result()
        #parking B
        parking = ParkingAgent(parking_name[1]+"@"+get_my_ip(),global_password,parking_spots=spotB_name,parking_letter="B",
                               parking_spots_state=parkingB_spots_state)
        self.parking_list.append(parking)
        future = parking.start()
        future.result()

        self.customer_handler = CustomerHandlerAgent(obsluga + "@" + get_my_ip(), global_password, parkings=["parkingA", "parkingB"],
                                               parkings_state=[["F", "F"], ["F", "F", "F"]],
                                               parkings_spots_delivery=[["N", "N"],["D", "N", "N"]],
                                               reservation_list=[[0, 0, 0, 1, 1, 0, 1, 2]])
        future = self.customer_handler.start()
        future.result()  # wait for receiver agent to be prepared.

        # driver
        self.driver = DriverAgent(kierowca1 + "@" + get_my_ip(), global_password, customer_handler_name=obsluga, driver_id="0",
                             is_delivery_man="N")
        future = self.driver.start()
        future.result()


def check_periodical_parking_change(agents):
    print("############## TEST OKRESOWEGO SPRAWDZENIA DOSTEPNOSCI ##############\n")
    start_at = datetime.datetime.now() + datetime.timedelta(seconds=3)
    wait_beh = agents.parking_list[0].InternalRequestParkingSpotsInfoBehav(period=60, start_at=start_at)
    agents.parking_list[0].add_behaviour(wait_beh)

def check_one_parking_test(agents):
    print("############## SPRAWDZENIE CALEGO PARKINGU ##############\n")
    wait_beh = agents.parking_list[0].RequestParkingSpotsInfoBehav()
    agents.parking_list[0].add_behaviour(wait_beh)

def send_parking_info_test(agents):
    print("############## TEST WYSLANIA INFORMACJI ##############\n")
    wait_beh = agents.driver.ReportLocationBehav()
    agents.driver.add_behaviour(wait_beh)

def do_reservation_test(agents):
    print("############## TEST REZERWACJI ##############\n")
    wait_beh = agents.driver.RequestReservationBehav(parking_id=0, parking_spot_id=1)
    agents.driver.add_behaviour(wait_beh)


def check_one_sensor_test(agents):
    print("############## TEST JEDNEGO CZUJNIKA ##############\n")
    wait_beh = agents.spotA_list[0].RequestParkingSpotInfoBehav()
    agents.spotA_list[0].add_behaviour(wait_beh)

def check_camera_test(agents):
    print("############## TEST JEDNEJ KAMERY ##############\n")
    wait_beh = agents.spotB_list[1].RequestParkingSpotInfoBehav()
    agents.spotB_list[1].add_behaviour(wait_beh)

def ask_for_parking_info_test(agents):
    print("############## POPROS O INFORMACJE PARKINGOWA ##############\n")
    wait_beh = agents.driver.RequestParkingInfoBehav(parking_id=0)
    agents.driver.add_behaviour(wait_beh)

def do_impossible_reservation_test(agents):
    print("############## SPROBUJ ZLOZYC REZERWACJE NA ZAJETE MIEJSCE ##############\n")
    wait_beh = agents.driver.RequestReservationBehav(parking_id=0, parking_spot_id=0)
    agents.driver.add_behaviour(wait_beh)

def do_courier_reservation_test(agents):
    print("############## SPROBUJ ZLOZYC REZERWACJE NA MIEJSCE KURIERA ##############\n")
    wait_beh = agents.driver.RequestReservationBehav(parking_id=1, parking_spot_id=0)
    agents.driver.add_behaviour(wait_beh)


def run_tests():
    my_agents = SystemAgents()
    time.sleep(5)
    check_one_sensor_test(my_agents)
    time.sleep(10)
    check_camera_test(my_agents)
    time.sleep(10)
    check_one_parking_test(my_agents)
    time.sleep(10)
    ask_for_parking_info_test(my_agents)
    time.sleep(10)
    do_reservation_test(my_agents)
    time.sleep(10)
    do_impossible_reservation_test(my_agents)
    time.sleep(10)
    do_courier_reservation_test(my_agents)
    time.sleep(10)
    send_parking_info_test(my_agents)
    time.sleep(10)
    check_periodical_parking_change(my_agents)
    time.sleep(10)


if __name__ == "__main__":
    run_tests()
    while True:
        if keyboard.is_pressed("q"):
            break
    print("Agents finished")
    quit_spade()
