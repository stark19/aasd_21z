import sys, threading

from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot, QThread
from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import QWidget, QMainWindow, QPushButton, QHBoxLayout, QVBoxLayout, QGridLayout, QLabel, QTextEdit, \
    QFrame
from math import sqrt

class OutLog:
    def __init__(self, edit, out=None, color=None):
        """(edit, out=None, color=None) -> can write stdout, stderr to a
        QTextEdit.
        edit = QTextEdit
        out = alternate stream ( can be the original sys.stdout )
        color = alternate color (i.e. color stderr a different color)
        """
        self.edit = edit
        self.out = None
        self.color = color

    def write(self, m):
        if self.color:
            tc = self.edit.textColor()
            self.edit.setTextColor(self.color)

        self.edit.moveCursor(QTextCursor.End)
        self.edit.insertPlainText( m )

        if self.color:
            self.edit.setTextColor(tc)

        if self.out:
            self.out.write(m)

class ParkingButton(QPushButton):

    def __init__(self, *args, parking_widget=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.parking_widget = parking_widget
        self.clicked.connect(self.update_marked_spot)

    def update_marked_spot(self):
        print("Wybrano miejsce: "+ self.text())
        self.parking_widget.recently_clicked_spot = self.text()


class ParkingWidget(QFrame):
    def __init__(self, *args, parking_spots=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.max_size_parking = 3
        self.recently_clicked_spot = None
        self.button_list = []
        self.parking_spots = parking_spots
        main_layout = QVBoxLayout()
        buttons_widget = QWidget()
        buttons_widget.setLayout(QVBoxLayout())
        self.generate_buttons(buttons_widget)
        main_layout.addWidget(QLabel("Parkings"), 10)
        main_layout.addWidget(buttons_widget, 1)
        self.setLayout(main_layout)

    def generate_buttons(self, buttons_widget):
        for parking in self.parking_spots:
            frame = QFrame()
            frame.setFrameStyle(1)
            grid_layout = QGridLayout()
            frame.setLayout(grid_layout)
            iterator = 0
            for i in range(self.max_size_parking):
                for j in range(self.max_size_parking):
                    button = ParkingButton(parking[iterator], parking_widget=self)
                    grid_layout.addWidget(button, i, j)
                    self.button_list.append(button)
                    iterator += 1
                    if iterator >= len(parking):
                        break
                if iterator >= len(parking):
                    break

            buttons_widget.layout().addWidget(frame)


class ConsoleWidget(QWidget):
    def __init__(self, *args, main_window=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_window = main_window
        layout = QVBoxLayout()
        self.setLayout(layout)
        layout.addWidget(QLabel("Logi wyjściowe"))
        self.text_widget = QTextEdit()
        self.text_widget.setReadOnly(True)
        layout.addWidget(self.text_widget)

    @pyqtSlot(str)
    def append_text(self, text):
        self.text_widget.moveCursor(QTextCursor.End)
        self.text_widget.insertPlainText(text)
        if "Received parking info:" in text:
            split1 = text.split(": ")
            spots_state = split1[2].split(",")
            self.main_window.update_buttons_from_console(spots_state)
        if "Reservation succesful" in text:
            self.main_window.update_buttons_from_console("Reserved")


class MainWindow(QMainWindow):
    def __init__(self, *args, parking_spots=None, LongRunningThing=None,driver=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.LongRunningThing = LongRunningThing
        self.driver = driver
        self.updated_info_state = False
        width = 1000
        height = 300
        self.setFixedSize(width, height)
        self.parking_spots = parking_spots
        self.main_widget = self.__prepare_main_widget()
        self.setWindowTitle("AASD_2021Z")
        self.setCentralWidget(self.main_widget)
        self.start_thread()
        self.red_button_color = "background-color : red"
        self.green_button_color = "background-color : green"
        self.yellow_button_color = "background-color : yellow"

    #@pyqtSlot()
    def start_thread(self):
        self.thread = QThread()
        self.long_running_thing = self.LongRunningThing()
        self.long_running_thing.moveToThread(self.thread)
        self.thread.started.connect(self.long_running_thing.run)
        self.thread.start()

    def __prepare_main_widget(self):
        ret = QWidget()
        ret_layout = QVBoxLayout()
        ret.setLayout(ret_layout)
        self.parking_widget = ParkingWidget(parking_spots=self.parking_spots)
        self.console = ConsoleWidget(main_window=self)
        parking_and_console_layout = QHBoxLayout()
        parking_and_console_layout.addWidget(self.parking_widget, 1)
        parking_and_console_layout.addWidget(self.console, 5)
        buttons_layout = QHBoxLayout()
        self.get_info_button = QPushButton("Pobierz informacje")
        self.make_reservation_button = QPushButton("Zrób rezerwację")
        self.get_info_button.clicked.connect(self.get_info_button_slot)
        self.make_reservation_button.clicked.connect(self.get_reservation_button_slot)
        buttons_layout.addWidget(self.get_info_button)
        buttons_layout.addWidget(self.make_reservation_button)
        ret_layout.addLayout(parking_and_console_layout)
        ret_layout.addLayout(buttons_layout)
        return ret

    def get_info_button_slot(self):
        self.driver.add_behaviour(self.driver.RequestParkingInfoBehav(parking_id=0))
        self.updated_info_state = True

    def get_reservation_button_slot(self):
        if self.updated_info_state:
            spot_id = None
            if self.parking_widget.recently_clicked_spot is not None:
                for indx, spot in enumerate(self.parking_spots[0]):
                    if spot == self.parking_widget.recently_clicked_spot:
                        spot_id = indx
                self.driver.add_behaviour(self.driver.RequestReservationBehav(parking_id=0, parking_spot_id=spot_id))
            else:
                print("Nie wybrano miejsca parkingowego")
        else:
            print("Brak informacji o parkingu")

    def update_buttons_from_console(self, button_state):
        if "Reserved" in button_state:
            reserved_spot = self.parking_widget.recently_clicked_spot
            for spot in self.parking_widget.button_list:
                if spot.text() == reserved_spot:
                    spot.setStyleSheet(self.yellow_button_color)
        elif len(button_state) != len(self.parking_widget.button_list):
            print("Couldn't update GUI info")
            return
        for index, state in enumerate(button_state):
            if state == "R":
                self.parking_widget.button_list[index].setStyleSheet(self.yellow_button_color)
            if state == "T":
                self.parking_widget.button_list[index].setStyleSheet(self.red_button_color)
            if state == "F":
                self.parking_widget.button_list[index].setStyleSheet(self.green_button_color)
