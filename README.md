Repozytorium przygotowane w ramach projektu z przedmiotu Agentowe i Aktorowe Systemy Decyzyjne.  

Autorzy:  
Bartłomiej Gołębiewski
Piotr Wałaszewski
Piotr Słysz  
  
  
Instrukcja uruchomienia:  
1. Projekt realizowany jest z użyciem pythonowego pakietu SPADE. Wymaga on rejestracji agentów na serwerze XMPP.
Zespół nasz korzystał z serwera serwera OpenFire który można pobrać tu: https://www.igniterealtime.org/downloads/#openfire
2. Po zainstalowaniu serwera potrzebna jest jego wstępna konfiguracja. Instrukcję tego jak to zrobić można znaleźć tutaj: https://youtu.be/JU2EA7j5lIc  
2a. Działanie serwera można sprawdzić logując się na konto za pomocą klienta np. Spark którego można pobrać z tego samego linku  
2b. Adresem serwera (domeną w polu Spark) jest nasz adres IP (można go znaleźć np. po wpisaniu ipconfig w cmd)  
2c. Przy konfiguracji serwera należy pamiętać żeby wybrać bazę 'embeded' aby automatycznie utworzyć bazę danych.
3. Dla poprawnego działania kodu należy zarejestrować odpowiednich użytkowników na serwerze. Ich lista znajduje się na górze pliku main.py
4. Po zakończeniu pracy warto sprawdzić w menedżerze zadań czy serwer nie jest przypadkiem cały czas online na naszym komputerze