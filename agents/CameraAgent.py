import datetime

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour
from spade.message import Message
from spade.template import Template

from tools import get_my_ip


class CameraAgent(Agent):
    def __init__(self, *args,parking_letter = None, spots = [], spots_state = [], **kwargs):
        super().__init__(*args, **kwargs)
        self.parking_letter = parking_letter
        self.spots = spots
        self.spots_state = spots_state
        
         
    def prepare_info(self):
        ret =''
        for key,val in self.sensor_state.items():
            ret+=key +" "+ val+" "
        return ret

    class AwaitBehav(CyclicBehaviour):
        async def run(self):
            msg = await self.receive(timeout=5)  # wait for a message for 5 seconds
            if msg:
                message = msg.body.split()
                if message[0]=="SensorInfoRequest":
                    print(self.agent.jid.localpart+": Received SensorInfoRequest")
                    print(self.agent.jid.localpart+": Preparing info about..." +message[1])
                    self.agent.add_behaviour(self.agent.InformParkingSpotBehav(mess_sender=msg.sender, message_body=self.agent.spots_state[self.agent.spots.index(message[1])]))

    class InformParkingSpotBehav(OneShotBehaviour):
        def __init__(self, *args, mess_sender=None, message_body=None, **kwargs):
            self.message_body = message_body
            self.mess_sender = mess_sender
            super().__init__(*args, **kwargs)

        async def run(self):
            msg = Message(to=self.mess_sender.localpart+'@'+self.mess_sender.domain)  # Instantiate the message
            msg.set_metadata("performative", "inform")
            msg.body =  self.message_body
            await self.send(msg)
            print(self.agent.jid.localpart+": Information sent!")

    async def setup(self):
        print("Camera started")
        Await = self.AwaitBehav()
        self.add_behaviour(Await)
