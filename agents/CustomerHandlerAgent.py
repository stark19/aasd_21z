import datetime

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour
from spade.message import Message
from spade.template import Template

from tools import get_my_ip


class CustomerHandlerAgent(Agent):
    def __init__(self, *args, parkings=None, parkings_state=None, parkings_spots_delivery=None, reservation_list=[], **kwargs):
        super().__init__(*args, **kwargs)
        self.parkings = parkings
        self.parkings_state = parkings_state
        self.parkings_spots_delivery = parkings_spots_delivery 
        self.reservation_list = reservation_list

    def prepare_parking_info(self, parking_id):
        for i in range(len(self.reservation_list)):
            park_id = self.reservation_list[i][0]
            spot_id = self.reservation_list[i][1]
            self.parkings_state[park_id][spot_id]="R"
        parking_info=""
        for i in range(len(self.parkings_state[parking_id])):
            parking_info=parking_info+self.parkings_state[parking_id][i]+","
        parking_info=parking_info[:-1]
        return parking_info

    class AwaitBehav(CyclicBehaviour):
        async def run(self):
            msg = await self.receive(timeout=5)  # wait for a message for 5 seconds
            if msg:
                message = msg.body.split()
                if message[0] == 'Reservation':
                    print(self.agent.jid.localpart+": Received reservation request")
                    #sprawdzenie czy uzytkownik nie ma kary
                    ##TO DO
                    #spawdzenie czy ma uprawnienia, zeby rezerwowac dane miejsce
                    if await(self.check_spot_permission(message[1], message[2], message[4])):
                        #sprawdzenie czy dane miejsce nie jest na liscie rezerwacji
                        if await(self.check_spot_reserved(message[1], message[2])):
                            #wykonaj sprawdzenie danego parkingu(na razie wszystkich)
                            wait_beh = self.agent.RequestParkingsInfoBehav()
                            self.agent.add_behaviour(wait_beh)
                            await wait_beh.join()
                            #zrob rezerwacje(lub odmowe)
                            if await(self.make_reservation(message[1], message[2], message[3])):
                                decision = "Response: accepted"
                            else:
                                decision = "Response: failed"
                        else:
                            decision = "Response: failed"
                    else:
                        decision = "Response: failed"
                    #wyslij odpowiedz do uzytkownika
                    self.agent.add_behaviour(self.agent.InformDriverBehav(mess_sender=msg.sender, message_body=decision))
                elif message[0] == 'ParkingInfo':
                    print(self.agent.jid.localpart+": Received ParkingInfo request")
                    wait_beh = self.agent.RequestParkingsInfoBehav()
                    self.agent.add_behaviour(wait_beh)
                    await wait_beh.join()
                    info = self.agent.prepare_parking_info(parking_id=int(message[1]))
                    self.agent.add_behaviour(self.agent.InformDriverBehav(mess_sender=msg.sender, message_body=info))
                elif message[0] == 'LocationInfo':
                    print(self.agent.jid.localpart+": Received LocationInfo from driver on spot with ID: "+message[1] )

        async def check_spot_reserved(self, parking_id, spot_id):
            print(self.agent.jid.localpart+": Checking spot is reserved!")
            for i in range(len(self.agent.reservation_list)):
                if self.agent.reservation_list[i][0]==int(parking_id) and self.agent.reservation_list[i][1]==int(spot_id):
                    print(self.agent.jid.localpart+": Spot is reserved!")
                    return False
            return True
        
        async def check_spot_permission(self, parking_id, spot_id, is_delivery_man):
            print(self.agent.jid.localpart+": Checking driver permission...")
            if(self.agent.parkings_spots_delivery[int(parking_id)][int(spot_id)]=="D" and is_delivery_man=="N"):
               print(self.agent.jid.localpart+": Driver doesn't have permission")
               return False
            else:
                print(self.agent.jid.localpart+": Driver has permission")
                return True

        async def make_reservation(self, parking_id, spot_id, driver_id):
            print(self.agent.jid.localpart+": Making reservation...")
            parking_id_val = int(parking_id)
            spot_id_val = int(spot_id)
            driver_id_val = int(driver_id)
            if(self.agent.parkings_state[parking_id_val][spot_id_val]=="F"):
                print(self.agent.jid.localpart+": Spot is free!")
                self.agent.reservation_list.append([parking_id_val,spot_id_val,driver_id_val,15])
                return True
            else:
                print(self.agent.jid.localpart+": Spot is taken!")
                return False

        async def RequestParkingInfo(self):
            pass

    class InformDriverBehav(OneShotBehaviour):
        def __init__(self, *args, mess_sender=None, message_body=None, **kwargs):
            self.message_body = message_body
            self.mess_sender = mess_sender
            super().__init__(*args, **kwargs)

        async def run(self):

            msg = Message(to=self.mess_sender.localpart+'@'+self.mess_sender.domain)  # Instantiate the message
            msg.set_metadata("performative", "inform")
            msg.body =  self.message_body
            await self.send(msg)
            print(self.agent.jid.localpart+": Information sent!")


    class RequestParkingsInfoBehav(OneShotBehaviour):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        async def run(self):
            print(self.agent.jid.localpart+": Making request for parkings info")
            i=0
            for parking_name in self.agent.parkings:
                msg = Message(to=parking_name + "@" + get_my_ip())  # Instantiate the message
                msg.set_metadata("performative", "request")
                msg.body = "ParkingInfoRequest"

                await self.send(msg)
                print(self.agent.jid.localpart+": ParkingInfo request sent!")
                msg = await self.receive(timeout=10)  # wait for a message for 10 seconds
                #self.agent.parkings_state[i]=msg.body
                if msg:
                    print(self.agent.jid.localpart+": Recevied parking info from parking "+parking_name+": " + msg.body)
                    message=list(msg.body.split(","))
                    for j in range(len(message)):
                        self.agent.parkings_state[i][j]=message[j]
                i=+1
                        
    class UpdateReservationTime(PeriodicBehaviour):
        async def run(self):
            index_list=[]
            print(self.agent.jid.localpart+": Updating Reservation Time!")
            for i in range(len(self.agent.reservation_list)):
                self.agent.reservation_list[i][3]-=1
                if(self.agent.reservation_list[i][3]==0):
                    #tu dorobic wpisanie kary lub zarejestrowac przewinienie
                    index_list.append(i)
            index_list.sort(reverse=True)
            for i in range(len(index_list)):
                print(self.agent.jid.localpart+": Reservation timeout!")
                self.agent.reservation_list.pop(index_list[i])
                


    async def setup(self):
        print("CustomerHandlerAgent started")
        Await = self.AwaitBehav()
        self.add_behaviour(Await)
