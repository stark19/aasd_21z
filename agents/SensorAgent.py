import datetime

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour
from spade.message import Message
from spade.template import Template

from tools import get_my_ip


class SensorAgent(Agent):
    def __init__(self, *args, sensor_state=None,spot=None, **kwargs):
        super().__init__(*args, **kwargs)
        if sensor_state==None:
            self.sensor_state = "F"
            self.spot = "F"
        else:
            self.sensor_state = sensor_state
            self.spot = spot

    class AwaitBehav(CyclicBehaviour):
        async def run(self):
            msg = await self.receive(timeout=5)  # wait for a message for 5 seconds
            if msg and msg.body=="SensorInfoRequest":
                print(self.agent.jid.localpart+": Received SensorInfoRequest")
                self.agent.add_behaviour(self.agent.InformParkingSpotBehav(mess_sender=msg.sender, message_body=self.agent.sensor_state))

    class InformParkingSpotBehav(OneShotBehaviour):
        def __init__(self, *args, mess_sender=None, message_body=None, **kwargs):
            self.message_body = message_body
            self.mess_sender = mess_sender
            super().__init__(*args, **kwargs)

        async def run(self):
            msg = Message(to=self.mess_sender.localpart+'@'+self.mess_sender.domain)  # Instantiate the message
            msg.set_metadata("performative", "inform")
            msg.body =  self.message_body
            await self.send(msg)
            print(self.agent.jid.localpart+": Information sent!")

    async def setup(self):
        print("Sensor started")
        Await = self.AwaitBehav()
        self.add_behaviour(Await)
