import datetime

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour
from spade.message import Message
from spade.template import Template

from tools import get_my_ip


class ParkingSpotAgent(Agent):
    def __init__(self, *args, parking_letter = None, spot = None, sensor_or_camera=None, spot_state=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.parking_letter = parking_letter
        self.spot = spot
        self.sensor_or_camera = sensor_or_camera
        self.spot_state = spot_state

    class AwaitBehav(CyclicBehaviour):
        async def run(self):
            msg = await self.receive(timeout=5)  # wait for a message for 5 seconds
            if msg and msg.body=="ParkingSpotInfoRequest":
                print(self.agent.jid.localpart+": Received ParkingSpotInfoRequest")
                wait_behav = self.agent.RequestParkingSpotInfoBehav()
                self.agent.add_behaviour(wait_behav)
                await wait_behav.join()
                self.agent.add_behaviour(self.agent.InformParkingBehav(mess_sender=msg.sender, message_body=self.agent.spot_state))

    class InformParkingBehav(OneShotBehaviour):
        def __init__(self, *args, mess_sender=None, message_body=None, **kwargs):
            self.message_body = message_body
            self.mess_sender = mess_sender
            super().__init__(*args, **kwargs)

        async def run(self):
            msg = Message(to=self.mess_sender.localpart+'@'+self.mess_sender.domain)  # Instantiate the message
            msg.set_metadata("performative", "inform")
            msg.body =  self.message_body
            await self.send(msg)
            print(self.agent.jid.localpart+": Information sent!")

    class RequestParkingSpotInfoBehav(OneShotBehaviour):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        async def run(self):
            print(self.agent.jid.localpart+": Making request for sensor info")
            msg = Message(to=self.agent.sensor_or_camera + "@" + get_my_ip())  # Instantiate the message
            msg.set_metadata("performative", "request")
            if (self.agent.sensor_or_camera[:-1]=="camera"):
                msg.body = "SensorInfoRequest " + self.agent.spot
            else:
                msg.body = "SensorInfoRequest"
            await self.send(msg)
            print(self.agent.jid.localpart+": SensorInfo request sent!")
            msg = await self.receive(timeout=10)  # wait for a message for 10 seconds
            if msg and msg.body=="F":
                self.agent.spot_state = "F"
                print(self.agent.jid.localpart+": Recevied info from sensor " + self.agent.sensor_or_camera + ": " + msg.body)
            else:
                self.agent.spot_state = "T"
                print(self.agent.jid.localpart+": Recevied info from sensor "+self.agent.sensor_or_camera + ": " + msg.body)

    async def setup(self):
        print("ParkingSpotAgent started")
        Await = self.AwaitBehav()
        self.add_behaviour(Await)
