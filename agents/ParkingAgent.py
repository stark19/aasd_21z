import datetime

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour
from spade.message import Message
from spade.template import Template

from tools import get_my_ip


class ParkingAgent(Agent):
    def __init__(self, *args, parking_spots=None, parking_letter=None, parking_spots_state=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.parking_spots = parking_spots
        self.parking_letter = parking_letter
        self.parking_spots_state = parking_spots_state

    def prepare_parking_info(self):
        parking_info=""
        for i in range(len(self.parking_spots_state)):
            parking_info=parking_info+self.parking_spots_state[i]+","
        parking_info=parking_info[:-1]
        return parking_info

    class AwaitBehav(CyclicBehaviour):
        async def run(self):
            msg = await self.receive(timeout=5)  # wait for a message for 5 seconds
            if msg and msg.body=="ParkingInfoRequest":
                print(self.agent.jid.localpart+": Received ParkingInfoRequest")
                wait_beh = self.agent.RequestParkingSpotsInfoBehav()
                self.agent.add_behaviour(wait_beh)
                await wait_beh.join()
                info = self.agent.prepare_parking_info()
                self.agent.add_behaviour(self.agent.InformCustomerHandlerBehav(mess_sender=msg.sender, message_body=info))

    class InternalRequestParkingSpotsInfoBehav(PeriodicBehaviour):
        async def run(self):
            print(self.agent.jid.localpart+ ": Internal RequestParkingSpotsInfoBehav sent!")
            wait_beh = self.agent.RequestParkingSpotsInfoBehav()
            self.agent.add_behaviour(wait_beh)
            await wait_beh.join()


    class InformCustomerHandlerBehav(OneShotBehaviour):
        def __init__(self, *args, mess_sender=None, message_body=None, **kwargs):
            self.message_body = message_body
            self.mess_sender = mess_sender
            super().__init__(*args, **kwargs)

        async def run(self):
            msg = Message(to=self.mess_sender.localpart+'@'+self.mess_sender.domain)  # Instantiate the message
            msg.set_metadata("performative", "inform")
            msg.body =  self.message_body
            await self.send(msg)
            print(self.agent.jid.localpart+": Information sent!")

    class RequestParkingSpotsInfoBehav(OneShotBehaviour):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
        
        async def run(self):
            print(self.agent.jid.localpart+": Making request for parking spot info")
            i=0
            for parking_spot_name in self.agent.parking_spots:
                msg = Message(to=parking_spot_name + "@" + get_my_ip())  # Instantiate the message
                msg.set_metadata("performative", "request")
                msg.body = "ParkingSpotInfoRequest"
                await self.send(msg)
                print(self.agent.jid.localpart+": ParkingSpotInfo request sent!")
                msg = await self.receive(timeout=10)  # wait for a message for 10 seconds
                self.agent.parking_spots_state[i]=msg.body
                i=i+1
                if msg:
                    print(self.agent.jid.localpart+": Recevied info from parking spot "+parking_spot_name+": " + msg.body)


    async def setup(self):
        print("ParkingAgent started")
        Await = self.AwaitBehav()
        self.add_behaviour(Await)
