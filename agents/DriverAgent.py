import datetime, time

from spade.agent import Agent
from spade.behaviour import CyclicBehaviour, PeriodicBehaviour, OneShotBehaviour
from spade.message import Message

from tools import get_my_ip


class DriverAgent(Agent):
    def __init__(self, *args, customer_handler_name=None,driver_id=None,is_delivery_man = None, **kwargs):
        self.customer_handler_name = customer_handler_name
        self.driver_id = driver_id
        self.is_delivery_man = is_delivery_man
        super().__init__(*args, **kwargs)

    class RequestReservationBehav(OneShotBehaviour):
        def __init__(self, *args, parking_id=None, parking_spot_id=None, **kwargs):
            self.parking_id = parking_id
            self.parking_spot_id = parking_spot_id
            super().__init__(*args, **kwargs)

        async def run(self):
            print(self.agent.jid.localpart+": Making reservation")
            msg = Message(to=self.agent.customer_handler_name+"@"+get_my_ip())     # Instantiate the message
            msg.set_metadata("performative", "request")
            msg.body = "Reservation "+str(self.parking_id)+" "+str(self.parking_spot_id)+" "+str(self.agent.driver_id)+" "+str(self.agent.is_delivery_man)

            await self.send(msg)
            print(self.agent.jid.localpart+": Reservation request sent!")
            msg = await self.receive(timeout=10)  # wait for a message for 10 seconds
            if msg:
                if msg.body == 'Response: accepted':
                    print(self.agent.jid.localpart+": Reservation succesful")
                else:
                    print(self.agent.jid.localpart+": Reservation failed")


    class RequestParkingInfoBehav(OneShotBehaviour):
        def __init__(self, *args, parking_id=None, **kwargs):
            self.parking_id = parking_id
            super().__init__(*args, **kwargs)

        async def run(self):
            print(self.agent.jid.localpart+": Making request for parking info")
            msg = Message(to=self.agent.customer_handler_name + "@" + get_my_ip())  # Instantiate the message
            msg.set_metadata("performative", "request")
            msg.body = "ParkingInfo " + str(self.parking_id)

            await self.send(msg)
            print(self.agent.jid.localpart+": Info request sent!")
            msg = await self.receive(timeout=10)  # wait for a message for 10 seconds
            if msg:
                print(self.agent.jid.localpart+": Received parking info: " + msg.body)


    class ReportLocationBehav(OneShotBehaviour):
        def __init__(self, *args, parking_spot_id=0, **kwargs):
            self.parking_spot_id = parking_spot_id
            super().__init__(*args, **kwargs)

        async def run(self):
            print(self.agent.jid.localpart+": Sending location parking info")
            msg = Message(to=self.agent.customer_handler_name + "@" + get_my_ip())  # Instantiate the message
            msg.set_metadata("performative", "info")
            msg.body = "LocationInfo " + str(self.parking_spot_id)

            await self.send(msg)
            print(self.agent.jid.localpart+": Location sent!")

    async def setup(self):
        print("DriverAgent started")